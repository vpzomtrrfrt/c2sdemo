function fetchWithError() {
	return fetch.apply(window, arguments)
		.then(res => {
			if(res.status < 200 || res.status >= 300) {
				return res.text().then(Promise.reject.bind(Promise));
			}
			return res;
		});
}

const selfURI = location.protocol + "//" + location.host + location.pathname;

function getApp(user) {
	const regEndpoint = user.endpoints.oauthRegistrationEndpoint;
	const storageKey = "c2sdemo_app_" + btoa(regEndpoint);
	if(storageKey in localStorage) {
		return Promise.resolve(JSON.parse(localStorage[storageKey]));
	}

	// As defined in Mastodon's API
	// https://docs.joinmastodon.org/methods/apps/#create-an-application
	const params = {
		client_name: "c2sdemo",
		redirect_uris: selfURI,
		scopes: "read write follow",
	};
	return fetchWithError(
		regEndpoint,
		{method: "POST", body: JSON.stringify(params), headers: {"Content-Type": "application/json"}},
	)
		.then(res => res.json())
		.then(app => {
			console.log(app);

			localStorage[storageKey] = JSON.stringify(app);

			return app;
		});
}

const loginform = document.getElementById("loginform");

loginform.addEventListener("submit", evt => {
	evt.preventDefault();

	fetchWithError(loginform.login_apid.value, {headers: {Accept: "application/activity+json"}})
		.then(res => res.json())
		.then(user => {
			getApp(user)
				.then(app => {
					location.assign(
						user.endpoints.oauthAuthorizationEndpoint +
							"?response_type=code&client_id=" + app.client_id +
							"&redirect_uri=" + encodeURIComponent(selfURI) +
							"&state=" + encodeURIComponent(user.id)
					);
				});
		})
		.catch(console.error);
});

if(location.search.indexOf("code=") >= 0) {
	loginform.style.display = "none";

	// definitely not how one should actually parse a query string
	const idx1 = location.search.indexOf("code=");
	let idx2 = location.search.indexOf("&", idx1);
	if(idx2 < 0) idx2 = undefined;
	const code = location.search.substring(idx1 + 5, idx2);

	const idx3 = location.search.indexOf("state=");
	let idx4 = location.search.indexOf("&", idx3);
	if(idx4 < 0) idx4 = undefined;
	const apid = decodeURIComponent(location.search.substring(idx3 + 6, idx4));

	fetchWithError(apid, {headers: {Accept: "application/activity+json"}})
		.then(res => res.json())
		.then(user => {
			// fetch app info
			const storageKey = "c2sdemo_app_" + btoa(user.endpoints.oauthRegistrationEndpoint);
			const app = JSON.parse(localStorage[storageKey]);

			const params = {
				grant_type: "authorization_code",
				code: code,
				client_id: app.client_id,
				client_secret: app.client_secret,
				redirect_uri: selfURI,
				scope: "read write follow",
			};

			return fetchWithError(
				user.endpoints.oauthTokenEndpoint,
				{method: "POST", body: JSON.stringify(params), headers: {"Content-Type": "application/json"}},
			)
				.then(res => res.json())
				.then(tokenInfo => {
					history.replaceState(null, "", location.pathname); // remove code from URL

					const token = tokenInfo.access_token;

					return fetchWithError(
						user.inbox,
						{headers: {Accept: "application/activity+json", Authorization: "Bearer " + token}},
					)
						.then(res => res.json())
						.then(inbox => {
							console.log(inbox);

							return fetchWithError(
								inbox.first,
								{headers: {Accept: "application/activity+json", Authorization: "Bearer " + token}},
							);
						})
						.then(res => res.json())
						.then(page => {
							console.log(page);

							page.orderedItems.forEach(item => {
								if(item.type === "Create" && item.object.type === "Note") {
									const div = document.createElement("div");

									const cite = document.createElement("cite");
									cite.textContent = item.actor;
									div.appendChild(cite);

									const quote = document.createElement("blockquote");
									quote.innerHTML = item.object.content;
									div.appendChild(quote);

									document.body.appendChild(div);
								}
							});
						});
				});
		})
		.catch(console.error);
}
